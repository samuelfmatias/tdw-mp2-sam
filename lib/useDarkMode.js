import { useEffect } from 'react';
import { usePrefersDarkMode } from './usePrefersDarkMode';
import { useSafeLocalStorage } from './useSafeLocalStorage';

/* copied from  https://dr-lucasleibs.medium.com/dark-mode-using-tailwindcss-react-hooks-1f0306cf149f */

function useDarkMode() {
  const prefersDarkMode = usePrefersDarkMode();
  const [isEnabled, setIsEnabled] = useSafeLocalStorage('dark-mode', undefined);

  const enabled = isEnabled === undefined ? prefersDarkMode : isEnabled;

  useEffect(() => {
    if (window === undefined) return;
    const root = window.document.documentElement;
    root.classList.remove(enabled ? 'light' : 'dark');
    root.classList.add(enabled ? 'dark' : 'light');
  }, [enabled]);

  return [enabled, setIsEnabled];
}

export default useDarkMode;
