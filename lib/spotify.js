import SpotifyWebApi from 'spotify-web-api-node/src/spotify-web-api';

const scopes = [
  'user-read-recently-played',
  'user-read-email',
  'playlist-read-private',
  'user-read-email',
  'user-library-read',
  'streaming',
  'user-read-playback-state',
  'user-top-read',
  'user-library-modify',
  'user-modify-playback-state',
  'user-read-currently-playing',
  'user-read-private',
  'user-follow-read',
].join(',');

const params = {
  scope: scopes,
};

/* https://developer.mozilla.org/pt-BR/docs/Web/API/URLSearchParams */

const queryParamString = new URLSearchParams(params);

export const URL_LOGIN = `https://accounts.spotify.com/authorize?${queryParamString.toString()}`;

export const spotifyAPI = new SpotifyWebApi({
  clientId: process.env.NEXT_PUBLIC_CLIENT_ID,
  clientSecret: process.env.NEXT_PUBLIC_CLIENT_SECRETNEXT_PUBLIC_CLIENT_ID,
});
