import { atom } from 'recoil';

export const SelectedPlaylistId = atom({
  key: 'SelectedPlaylistId',
  default: null,
});
export const SelectedPlaylistDetails = atom({
  key: 'SelectedPlaylistDetails',
  default: null,
});
