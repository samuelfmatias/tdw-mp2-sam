import { atom } from 'recoil';

export const selectedTrackId = atom({
  key: 'selectedTrackIdState',
  default: null,
});
export const isPlaying = atom({
  key: 'isPlayingState',
  default: false,
});
