import { atom } from 'recoil';

export const TrackResults = atom({
  key: 'TrackResultsState',
  default: '',
});
export const AlbumResults = atom({
  key: 'AlbumResultsState',
  default: '',
});
export const SearchField = atom({
  key: 'SearchFieldState',
  default: '',
});
