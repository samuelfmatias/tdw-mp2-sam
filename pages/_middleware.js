import { getToken } from 'next-auth/jwt';
import { NextResponse } from 'next/server';

export async function middleware(req) {
  const token = await getToken({
    req,
    secret: process.env.JWT_SECRET,
  }); /* Se o user estiver logado, terá um token válido */

  const { pathname } = req.nextUrl; //Lê o url do pedido que foi feito

  /* 1) Se o user já estiver logado e estiver a tentar aceder ao login, redirecionar para a homepage/dashboard */
  if (token && pathname.includes('/login')) {
    return NextResponse.redirect('/dashboard');
  }

  /* 2) e se o url a que está a tentar aceder for de autenticação ou se existir um token válido -> Continua a execução normal */
  if (pathname.includes('/api/auth') || token) {
    return NextResponse.next();
  }

  /* 3) Se não existir um token e se estiverem a tentar aceder a um url 'privado' */
  if (!token && pathname !== '/login') {
    /* Impedir esse pedido e enviar o user para a página de login */
    return NextResponse.redirect('/login');
  }
}
