import React from 'react';
import '../styles/globals.css';
import PropTypes from 'prop-types';
import { SessionProvider } from 'next-auth/react';
import { RecoilRoot } from 'recoil';
import Head from 'next/head';

/* SESSION Based on NextAuth v4 https://next-auth.js.org/getting-started/upgrade-v4#sessionprovider */
function MyApp({ Component, pageProps: { session, ...pageProps } }) {
  return (
    <SessionProvider session={session}>
      {/* Acesso ao state global em toda a aplicação */}
      <RecoilRoot>
        <>
          <Head>
            <title>Spotify</title>
            <link rel='icon' href='/favicon.ico' />
            <link rel='apple-touch-icon' sizes='180x180' href='/apple-touch-icon.png' />
            <link rel='icon' type='image/png' sizes='32x32' href='/favicon-32x32.png' />
            <link rel='icon' type='image/png' sizes='16x16' href='/favicon-16x16.png' />
            <link rel='mask-icon' href='/safari-pinned-tab.svg' color='#28cc5b' />
            <meta name='msapplication-TileColor' content='#28cc5b'></meta>
            <meta name='theme-color' content='#28cc5b'></meta>
          </Head>
          <div className='dark:bg-[#121212] h-screen overflow-hidden'>
            <Component {...pageProps} />
          </div>
        </>
      </RecoilRoot>
    </SessionProvider>
  );
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object,
};

export default MyApp;
