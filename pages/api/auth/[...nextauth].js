import NextAuth from 'next-auth';
import SpotifyProvider from 'next-auth/providers/spotify';
import { spotifyAPI, URL_LOGIN } from '../../../lib/spotify';

export default NextAuth({
  providers: [
    SpotifyProvider({
      clientId: process.env.NEXT_PUBLIC_CLIENT_ID,
      clientSecret: process.env.NEXT_PUBLIC_CLIENT_SECRET,
      authorization: URL_LOGIN,
    }),
    // ...add more providers here if I have the time to do so
  ],
  secret: process.env.JWT_SECRET,
  pages: {
    signIn: '/login',
  },
  /* Logica de Access Token & Refresh Token  */
  callbacks: {
    async jwt({ token, account, user }) {
      /*  Na primeira vez que faço login*/
      /* Spotify refreshes token after 3600s (1h) */
      if (account && user) {
        return {
          ...token,
          username: account.providerAccountId,
          refreshToken: account.refresh_token,
          accessToken: account.access_token,
          accessTokenExpires: account.expires_at * 1000, // milliseconds - 1h depois do início da validade
        };
      }
      /* Se não for a 1a vez, verificar se o token ainda é válido */
      if (Date.now() < token.accessTokenExpires) {
        // quer dizer que o token está válido
        return token;
      }
      /* Se não for, dar refresh ao mesmo */
      console.log('access token refreshing');
      return await refreshMyAccessToken(token);
    },

    async session({ session, token }) {
      session.user.username = token.username;
      session.user.refreshToken = token.refreshToken;
      session.user.accessToken = token.accessToken;

      return session;
    },
  },
});

async function refreshMyAccessToken(token) {
  try {
    spotifyAPI.setAccessToken(token.accessToken);
    spotifyAPI.setRefreshToken(token.refreshToken);
    const newToken = await spotifyAPI.getRefreshToken();
    return {
      ...token,
      accessToken: newToken.access_token,
      accessTokenExpires: newToken.expires_in * 1000 + Date.now,
      /* Se vier um novo refresehToken -> usa-o // Se não -> usa o antigo */
      refreshToken: newToken.refresh_token ? newToken.refresh_token : token.refreshToken,
    };
  } catch (error) {
    console.log(error);
    return {
      ...token,
      error: 'RefreshTokenError',
    };
  }
}
