import React from 'react';
import Sidebar from '../components/sidebar/Sidebar';
import { getSession } from 'next-auth/react';
import { spotifyAPI } from '../lib/spotify';
import propTypes from 'prop-types';
import _ContentUserHeader from '../components/UserHeader';
import PageNavigation from '../components/PageNavigation';
import SearchInput from '../components/SearchInput';
import { useRecoilState } from 'recoil';
import { AlbumResults, TrackResults } from '../atoms/searchAtom';
import Player from '../components/Player';
import SongResults from '../components/TrackResults';
import PlaylistResults from '../components/PlaylistResults';
export default function Search({ myPlaylists }) {
  const [trackResults] = useRecoilState(TrackResults);
  const [albumsResults] = useRecoilState(AlbumResults);

  return (
    <>
      <main className='flex overflow-y-hidden' style={{ height: '90vh' }}>
        <Sidebar myPlaylists={myPlaylists} />
        <div className='flex-grow dark:text-white overflow-y-auto w-full'>
          <PageNavigation />
          <SearchInput />
          <_ContentUserHeader />
          {trackResults !== '' ? (
            <>
              <SongResults trackResults={trackResults} />
              <PlaylistResults playlistResults={albumsResults} />
            </>
          ) : (
            <h1 className='text-xl pt-28 px-10 font-extrabold truncate md:text-3xl lg:text-5xl xl:text-9xl'>
              SEARCH SOMETHING
            </h1>
          )}
        </div>
      </main>

      <div className='sticky bottom-0'>
        <Player />
      </div>
    </>
  );
}

Search.propTypes = {
  myPlaylists: propTypes.array,
};

export async function getServerSideProps(context) {
  const session = await getSession(context);
  spotifyAPI.setAccessToken(session.user.accessToken);
  /* GET USER'S PLAYLISTS */
  const myPlaylists = await spotifyAPI.getUserPlaylists({ limit: 50 }).then(
    (data) => data.body.items,
    (err) => err.body,
  );
  return {
    props: {
      session,
      myPlaylists,
    },
  };
}
