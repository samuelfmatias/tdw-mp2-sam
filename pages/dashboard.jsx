import React from 'react';
import Sidebar from '../components/sidebar/Sidebar';
import { getSession } from 'next-auth/react';
import { spotifyAPI } from '../lib/spotify';
import propTypes from 'prop-types';
import _ContentUserHeader from '../components/UserHeader';
import PageNavigation from '../components/PageNavigation';
import Player from '../components/Player';

export default function Dashboard({ myPlaylists }) {
  return (
    <>
      <main className='flex overflow-y-hidden' style={{ height: '90vh' }}>
        <Sidebar myPlaylists={myPlaylists} />
        <div className='flex-grow dark:text-white overflow-y-auto w-full'>
          <PageNavigation />
          <_ContentUserHeader />

          <h1 className='text-3xl pt-28 px-10 font-extrabold truncate text-center text-green-500'>
            WELCOME TO THE SHITTY VERSION OF SPOTIFY
          </h1>
          <h2 className='text-2xl pt-20 px-10 font-extrabold truncate text-center'>
            In order for this to work you have to open an official Spotify instance
          </h2>
          <h2 className='text-2xl pt-2 px-10 font-extrabold truncate text-center'>
            This web application only acts as a remote for your device
          </h2>
          <h3 className='text-xl pt-20 px-10 font-extrabold truncate text-center'>
            Whenever you do so ... refresh this page
          </h3>
          <h4 className='text-xl pt-[25vh] px-10 font-extrabold truncate text-center'>
            {`If everything is ready ... You'll be able to see the song that is playing down in the player bellow`}
          </h4>
        </div>
      </main>
      <div className='sticky bottom-0'>
        <Player />
      </div>
    </>
  );
}

Dashboard.propTypes = {
  myPlaylists: propTypes.array,
};

export async function getServerSideProps(context) {
  const session = await getSession(context);
  spotifyAPI.setAccessToken(session.user.accessToken);
  /* GET USER'S PLAYLISTS */
  const myPlaylists = await spotifyAPI.getUserPlaylists({ limit: 50 }).then(
    (data) => data.body.items,
    (err) => err.body,
  );
  return {
    props: {
      session,
      myPlaylists,
    },
  };
}
