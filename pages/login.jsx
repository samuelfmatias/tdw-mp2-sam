import React from 'react';
import propTypes from 'prop-types';
import { getProviders, signIn } from 'next-auth/react';

function Login({ providers: { spotify } }) {
  /*  https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Object/values */
  return (
    <>
      <div className='min-h-screen w-full flex flex-col bg-black justify-center'>
        <button
          className='bg-[#18D860] text-white p-5 rounded-lg'
          onClick={() => signIn(spotify.id, { callbackUrl: '/' })}
        >
          Login with {spotify.name}
        </button>
      </div>
    </>
  );
}

Login.propTypes = {
  providers: propTypes.object,
};

export default Login;

export async function getServerSideProps() {
  const providers = await getProviders();
  return {
    props: {
      providers,
    },
  };
}
