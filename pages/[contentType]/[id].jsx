import React, { useEffect } from 'react';
import _ContentContextHeader from '../../components/ContextHeader';
import _ContentUserHeader from '../../components/UserHeader';
import { useRecoilState } from 'recoil';
import { SelectedPlaylistDetails, SelectedPlaylistId } from '../../atoms/playlistsAtom';
import useSpotify from '../../components/hooks/useSpotify.js';
import Sidebar from '../../components/sidebar/Sidebar';
import propTypes from 'prop-types';
import { getSession } from 'next-auth/react';
import { spotifyAPI } from '../../lib/spotify';
import { useRouter } from 'next/router';
import SongList from '../../components/SongList';
import Player from '../../components/Player';
import PageNavigation from '../../components/PageNavigation';
function Content({ myPlaylists }) {
  const spotifyAPI = useSpotify();
  const router = useRouter();
  const [selectedPlaylistId, setSelectedPlaylistId] = useRecoilState(SelectedPlaylistId);
  const [selectedPlaylistDetails, setSelectedPlaylistDetails] =
    useRecoilState(SelectedPlaylistDetails);

  /* Se n houver state e estivermos nesta página é porque houve um reload - voltar a definir o state*/
  useEffect(() => {
    setSelectedPlaylistId(router.query.id);
  }, [router]);

  useEffect(() => {
    selectedPlaylistId &&
      spotifyAPI
        .getPlaylist(selectedPlaylistId)
        .then((data) => {
          setSelectedPlaylistDetails(data.body);
        })
        .catch((error) =>
          console.log(
            'Error Fetching Playlist with ID -> ',
            selectedPlaylistId,
            'ERROR -> ',
            error,
          ),
        );
  }, [selectedPlaylistId]);

  return (
    <>
      <main className='flex overflow-y-hidden' style={{ height: '90vh' }}>
        <Sidebar myPlaylists={myPlaylists} />
        <div className='flex-grow dark:text-white overflow-y-auto w-full'>
          <PageNavigation />
          <_ContentUserHeader />
          <_ContentContextHeader selectedPlaylistDetails={selectedPlaylistDetails} />
          <SongList />
        </div>
      </main>

      <div className='sticky bottom-0'>
        <Player />
      </div>
    </>
  );
}

export default Content;

Content.propTypes = {
  myPlaylists: propTypes.array,
};

export async function getServerSideProps(context) {
  const session = await getSession(context);
  spotifyAPI.setAccessToken(session.user.accessToken);
  /* GET USER'S PLAYLISTS TO SIDEBAR*/
  const myPlaylists = await spotifyAPI.getUserPlaylists({ limit: 50 }).then(
    (data) => data.body.items,
    (err) => console.log('Something went wrong!', err),
  );
  return {
    props: {
      session,
      myPlaylists,
    },
  };
}
