import React from 'react';
import propTypes from 'prop-types';
import Song from './Song';

function SongResults({ trackResults }) {
  return (
    <section className={`flex flex-col pt-28 pl-8 items-end space-x-7`}>
      <div className='flex flex-col text-white pl-0 pr-16 pt-5 w-[95%] border-b-2 border-b-gray-500 m-auto'>
        <h1 className='font-bold text-3xl pb-5'>Songs</h1>
      </div>
      <div className='flex flex-col text-white px-8 pr-16 pt-5 w-full'>
        {trackResults?.tracks?.items.map((track, number) => (
          <Song key={track.id} track={{ track: track }} number={number} />
        ))}
      </div>
    </section>
  );
}
SongResults.propTypes = {
  trackResults: propTypes.object,
};

export default SongResults;
