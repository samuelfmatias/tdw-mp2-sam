import { useRouter } from 'next/router';
import propTypes from 'prop-types';
import React from 'react';
import { useRecoilState } from 'recoil';
import { SelectedPlaylistId } from '../atoms/playlistsAtom';
import { SearchField } from '../atoms/searchAtom';

function PlaylistResults({ playlistResults }) {
  const router = useRouter();
  const [, setSelectedPlaylistId] = useRecoilState(SelectedPlaylistId);
  const [, setSearch] = useRecoilState(SearchField);
  return (
    <section className={`flex flex-col pl-8 items-end space-x-7`}>
      <div className='flex flex-col text-white pl-0 pr-16 pt-5 w-[95%] border-b-2 border-b-gray-500 m-auto'>
        <h1 className='font-bold text-3xl pb-5'>Playlists</h1>
      </div>
      <div className='flex text-white pb-20 px-8 pr-16 pt-5 w-full overflow-x-scroll'>
        {playlistResults?.playlists?.items?.map((playlist) => (
          <div
            className='flex flex-col align-center justify-center px-4 py-8 text-left hover:bg-zinc-800 hover:cursor-pointer rounded-sm'
            key={playlist.id}
            onClick={() => {
              setSelectedPlaylistId(playlist.id);
              setSearch('');
              router.push(`/${playlist.type}/${playlist.id}`);
            }}
          >
            <img src={playlist?.images?.[0].url} alt='Playlist Cover' className='max-w-[10rem]' />
            <h3 className='pl-2 mt-4'>{playlist.name}</h3>
            <h4 className='pl-2 mt-1 text-xs text-zinc-400'>By {playlist.owner.display_name}</h4>
          </div>
        ))}
      </div>
    </section>
  );
}
PlaylistResults.propTypes = {
  playlistResults: propTypes.object,
};

export default PlaylistResults;
