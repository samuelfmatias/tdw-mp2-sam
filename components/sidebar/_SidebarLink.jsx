import React from 'react';
import propTypes from 'prop-types';

function SidebarLink({ Icon, title, isActive, action = () => {} }) {
  return (
    <button
      className={`flex items-center space-x-2 dark:hover:text-white hover:text-black transition ease-linear duration-200 ${
        isActive ? 'text-white font-semibold' : ''
      }`}
      onClick={action}
    >
      <Icon className='h-6 w-6' />
      {title && <p className='font-medium'>{title}</p>}
    </button>
  );
}

SidebarLink.propTypes = {
  Icon: propTypes.func,
  action: propTypes.func,
  title: propTypes.string,
  isActive: propTypes.bool,
};

export default SidebarLink;
