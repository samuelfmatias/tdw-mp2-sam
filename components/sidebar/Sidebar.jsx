import React, { useEffect } from 'react';
import { MdHomeFilled, MdLightMode } from 'react-icons/md';
import { BsFillMoonFill } from 'react-icons/bs';
import { FiSearch } from 'react-icons/fi';
import { RiLogoutBoxRLine } from 'react-icons/ri';
import SidebarLink from './_SidebarLink';
import useDarkMode from '../../lib/useDarkMode';
import { signOut } from 'next-auth/react';
import { useRecoilState } from 'recoil';
import { SelectedPlaylistId } from '../../atoms/playlistsAtom';
import propTypes from 'prop-types';
import { useRouter } from 'next/router';

function Sidebar({ myPlaylists }) {
  const router = useRouter();

  const [isDark, setIsDark] = useDarkMode();
  const [selectedPlaylistId, setSelectedPlaylistId] = useRecoilState(SelectedPlaylistId);

  useEffect(() => {}, [selectedPlaylistId]);
  /*  useEffect(() => {
    if (spotifyAPI.getAccessToken()) {
      spotifyAPI.getUserPlaylists({ limit: 50 }).then(
        function (data) {
          console.log('Retrieved playlists', data.body.items);
          setPlaylists(data.body.items);
        },
        function (err) {
          console.log('Something went wrong!', err);
        },
      );
    }
  }, [session]); */

  return (
    <div className='flex-col dark:text-gray-400 text-gray-700 p-5 text-xs lg:text-sm border-r border-gray-900 space-y-4 dark:bg-black max-w-[12rem] lg:max-w-[15rem] hidden md:inline-flex overflow-y-scroll'>
      <div className='flex flex-col gap-4'>
        {/* MENU */}
        <SidebarLink
          Icon={MdHomeFilled}
          title='Home'
          action={() => router.push('/dashboard')}
          isActive={router.pathname === '/dashboard'}
        />
        <SidebarLink
          Icon={FiSearch}
          title='Search'
          action={() => router.push('/search')}
          isActive={router.pathname === '/search'}
        />

        <SidebarLink
          Icon={isDark ? MdLightMode : BsFillMoonFill}
          title={isDark ? 'Light Mode' : 'Dark Mode'}
          action={() => setIsDark(!isDark)}
        />
        <SidebarLink Icon={RiLogoutBoxRLine} title='Logout' action={() => signOut()} />
      </div>
      <hr className='border-t-[0.1px] border-gray-900' />
      <section className='overflow-y-auto space-y-4 h-[75%]'>
        {/* PLAYLISTS */}
        {myPlaylists.error ? (
          <p>Error: {myPlaylists.error.message}</p>
        ) : (
          myPlaylists.map((playlist) => (
            <p
              key={playlist.id}
              onClick={() => {
                setSelectedPlaylistId(playlist.id);
                router.push(`/${playlist.type}/${playlist.id}`);
              }}
              className={`cursor-pointer dark:hover:text-white hover:text-black transition ease-linear duration-200 ${
                selectedPlaylistId === playlist.id &&
                router.pathname.split('/')[1] === '[contentType]'
                  ? 'text-white font-semibold'
                  : ''
              }`}
            >
              {playlist.name}
            </p>
          ))
        )}
      </section>
    </div>
  );
}

Sidebar.propTypes = {
  myPlaylists: propTypes.array,
};
export default Sidebar;
