import React, { useEffect } from 'react';
import propTypes from 'prop-types';

function _ContentContextHeader({ selectedPlaylistDetails }) {
  useEffect(() => {
    console.log('teste', selectedPlaylistDetails);
  }, [selectedPlaylistDetails]);
  return (
    <>
      <section
        className={`flex bg-gradient-to-b dark:to-[#121212] to-white from-[gray] dark:from-[lightgray] dark:text-white pt-28 pl-8 items-end space-x-7`}
      >
        {selectedPlaylistDetails && (
          <>
            <img
              className='h-48 w-48 shadow-xl'
              src={selectedPlaylistDetails.images[0].url}
              alt='Playlist Image'
            />
            <div className='flex flex-col self-center gap-3 pt-11'>
              <p className='text-xs uppercase'>{selectedPlaylistDetails.type}</p>
              <h1 className='font-extrabold text-7xl'>{selectedPlaylistDetails.name}</h1>
              <p className='text-sm font-bold'>
                {selectedPlaylistDetails.owner.display_name}{' '}
                <span className=' text-gray-400 pl-2 pr-2'>•</span>
                {selectedPlaylistDetails.followers.total > 0 && (
                  <>
                    <span className=' text-gray-400 font-normal'>
                      {selectedPlaylistDetails.followers.total} likes
                    </span>
                    <span className=' text-gray-400 pl-2 pr-2'>•</span>
                  </>
                )}
                <span className=' text-gray-400 font-normal'>
                  {selectedPlaylistDetails.tracks.total} songs
                </span>
              </p>
            </div>
          </>
        )}
      </section>
    </>
  );
}

_ContentContextHeader.propTypes = {
  selectedPlaylistDetails: propTypes.object,
};
export default _ContentContextHeader;
