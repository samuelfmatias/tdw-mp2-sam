import React, { useCallback, useEffect, useState } from 'react';
import useSpotify from './hooks/useSpotify.js';
import useSongDetails from './hooks/useSongDetails.js';
import /* useSession */ 'next-auth/react';
import { useRecoilState } from 'recoil';
import { isPlaying /* selectedTrackId */ } from '../atoms/songAtom.js';
/* ICONS */
import { BsFillSkipEndFill, BsFillSkipStartFill } from 'react-icons/bs';
import { AiFillPlayCircle, AiFillPauseCircle } from 'react-icons/ai';
import { ImShuffle } from 'react-icons/im';
import { FiRepeat } from 'react-icons/fi';
import { BsFillVolumeDownFill, BsFillVolumeMuteFill } from 'react-icons/bs';
import { debounce } from 'lodash';
/* import { MdOutlineRepeatOne, MdOutlineDevicesOther } from 'react-icons/md';
 */
function Player() {
  const spotifyAPI = useSpotify();
  /*   const { data: session, status } = useSession();
   */ /*   const [currentTrackId, setCurrentTrackId] = useRecoilState(selectedTrackId);
   */ const [isPlayingState, setIsPlayingState] = useRecoilState(isPlaying);
  const [volume, setVolume] = useState(75);
  const songDetails = useSongDetails();

  const handleControl = () => {
    spotifyAPI
      .getMyCurrentPlaybackState()
      .then((data) => {
        if (data.body.is_playing) {
          spotifyAPI.pause();
          setIsPlayingState(false);
        } else {
          spotifyAPI.play();
          setIsPlayingState(true);
        }
      })
      .catch((e) => console.log(e));
  };

  useEffect(() => {
    if (volume >= 0 && volume <= 100) {
      debouncVolume(volume);
    }
  }, [volume]);

  const debouncVolume = useCallback(
    debounce((volume) => {
      spotifyAPI.setVolume(volume).catch((e) => console.log(e));
    }, 200),
    [],
  );
  return (
    <nav
      className='bg-zinc-900 text-white grid grid-cols-3 px-2 md:px-7 border-t border-[#282828]'
      style={{ height: '10vh' }}
    >
      {songDetails ? (
        <>
          <section className='flex items-center space-x-4  text-sm md:text-base '>
            <img
              className='flex w-16 h-16 justify-self-center'
              src={songDetails?.album?.images?.[0].url}
              alt='Current Playing Song Album Cover'
            />
            <div>
              <h3>{songDetails?.name}</h3>
              <p className='text-xs text-gray-400'>{songDetails?.artists?.[0]?.name}</p>
            </div>
          </section>

          <section className='flex items-center justify-evenly'>
            <ImShuffle className='mediaControls' />
            <BsFillSkipStartFill className='mediaControls w-8 h-8' />
            {isPlayingState ? (
              <AiFillPauseCircle onClick={handleControl} className='mediaControls w-10 h-10' />
            ) : (
              <AiFillPlayCircle onClick={handleControl} className='mediaControls w-10 h-10' />
            )}
            <BsFillSkipEndFill className='mediaControls w-8 h-8' />
            <FiRepeat className='mediaControls' />
          </section>

          <section className='flex items-center space-x-3 md:space-x-4 justify-end'>
            {volume > 0 ? (
              <BsFillVolumeDownFill className='mediaControls' onClick={() => setVolume(0)} />
            ) : (
              <BsFillVolumeMuteFill className='mediaControls' onClick={() => setVolume(50)} />
            )}

            <input
              className='w-14 md:w-28'
              type='range'
              min={0}
              max={100}
              value={volume}
              onChange={(e) => setVolume(Number(e.target.value))}
            />
          </section>
        </>
      ) : (
        <h2 className='flex items-center space-x-4  text-xs'>
          ⚠️ Loading ... Open Original Spotify Instance , and then reload this page
        </h2>
      )}
    </nav>
  );
}

export default Player;
