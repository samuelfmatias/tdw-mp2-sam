import React from 'react';
import { useSession } from 'next-auth/react';
import { MdNavigateBefore } from 'react-icons/md';
import { useRouter } from 'next/router';

function PageNavigation() {
  const { data: session } = useSession();
  const router = useRouter();
  return (
    <>
      {session && (
        <nav className='flex items-center absolute top-5 left-72  h-fit font-extrabold'>
          <div
            onClick={() => router.back()}
            className=' dark:bg-[#484848]  text-gray-200 bg-gray-500 shadow-xl space-x-3 opacity-90 dark:hover:opacity-80 hover:opacity-100 cursor-pointer rounded-full p-1 pr-2 w-10 h-10 flex items-center justify-center mr-4'
          >
            <MdNavigateBefore className='text-5xl' />
          </div>
        </nav>
      )}
    </>
  );
}

export default PageNavigation;
