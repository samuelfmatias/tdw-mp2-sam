import React from 'react';
import { useRecoilValue } from 'recoil';
import { SelectedPlaylistDetails } from '../atoms/playlistsAtom';
import Song from './Song';
function SongList() {
  const selectedPlaylistDetails = useRecoilValue(SelectedPlaylistDetails);
  return (
    <div className='flex flex-col text-white pb-20 px-8 pt-10'>
      {selectedPlaylistDetails?.tracks.items.map((track, number) => (
        <Song key={track.track.id} track={track} number={number} />
      ))}
    </div>
  );
}

export default SongList;
