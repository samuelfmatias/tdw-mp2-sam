import React from 'react';
import propTypes from 'prop-types';
import { millisToMinutesAndSeconds } from '../lib/timeConvertion';
import { useRecoilState } from 'recoil';
import { selectedTrackId, isPlaying } from '../atoms/songAtom';
import { spotifyAPI } from '../lib/spotify';
function Song({ track, number }) {
  const [currentTrackId, setCurrentTrackId] = useRecoilState(selectedTrackId);
  const [, setIsPlayingState] = useRecoilState(isPlaying);

  const playTrack = () => {
    setCurrentTrackId(track.track.id);
    setIsPlayingState(true);
    spotifyAPI.play({ uris: [track.track.uri] });
  };

  return (
    <article
      onClick={playTrack}
      className='text-gray-400 text-sm hover:bg-opacity-70 hover:bg-zinc-800 rounded-md cursor-pointer grid grid-cols-2 py-2 px-6'
    >
      <div className='flex items-center space-x-4'>
        <p>{number + 1}</p>
        <img
          className='w-10 h-10'
          src={track.track.album.images[0].url}
          alt="Music's album cover"
        />
        <div>
          <p
            className={`w-40 lg:w-64 truncate ${
              currentTrackId === track.track.id ? 'text-green-500 font-semibold' : 'text-white'
            } text-md`}
          >
            {track.track.name}
          </p>
          <p className='w-40'>{track.track.artists[0].name}</p>
        </div>
      </div>
      <div className='flex items-center ml-auto justify-between md:ml-0'>
        <p className='w-40 hidden md:inline'>{track.track.album.name}</p>
        <p>{millisToMinutesAndSeconds(track.track.duration_ms)}</p>
      </div>
    </article>
  );
}
Song.propTypes = {
  track: propTypes.object,
  number: propTypes.number,
};
export default Song;
