import React, { useCallback, useEffect } from 'react';
import { useSession } from 'next-auth/react';
import { debounce } from 'lodash';
import useSpotify from './hooks/useSpotify';
import { useRecoilState } from 'recoil';
import { AlbumResults, SearchField, TrackResults } from '../atoms/searchAtom';

function SearchInput() {
  const spotifyAPI = useSpotify();

  const { data: session } = useSession();
  const [search, setSearch] = useRecoilState(SearchField);
  const [, setTrackResults] = useRecoilState(TrackResults);
  const [, setAlbumsResults] = useRecoilState(AlbumResults);

  useEffect(() => {
    debouncSearch(search);
  }, [search]);

  const debouncSearch = useCallback(
    debounce((search) => {
      search !== ''
        ? (spotifyAPI.searchTracks(search, { limit: 5 }).then(
            function (data) {
              console.log(data);
              setTrackResults(data.body);
            },
            function (err) {
              console.error(err);
            },
          ),
          spotifyAPI.searchPlaylists(search, { limit: 5 }).then(
            function (data) {
              console.log(data);
              setAlbumsResults(data.body);
            },
            function (err) {
              console.log('Something went wrong!', err);
            },
          ))
        : (setAlbumsResults(search), setTrackResults(search));
    }, 500),
    [],
  );
  return (
    <>
      {session && (
        <input
          onChange={(e) => setSearch(e.target.value)}
          className='flex items-center absolute top-5 w-[25rem] left-96 min-h-[40px] rounded-full px-8 hover:outline-none focus:outline-none text-sm bg-white placeholder:text-zinc-400 placeholder:font-semibold text-black font-semibold'
          type='text'
          value={search}
          placeholder='Search for songs, albums and artists'
        />
      )}
    </>
  );
}

export default SearchInput;
