import { useEffect, useState } from 'react';
import { useRecoilState } from 'recoil';
import { isPlaying, selectedTrackId } from '../../atoms/songAtom';
import useSpotify from './useSpotify';

function useSongDetails() {
  const spotifyAPI = useSpotify();
  const [currentTrackId, setCurrentTrackId] = useRecoilState(selectedTrackId);
  const [songDetails, setSongDetails] = useState(null);
  const [, setIsPlayingState] = useRecoilState(isPlaying);

  useEffect(() => {
    const fetchSongDetails = async () => {
      let trackDetails;
      if (currentTrackId) {
        trackDetails = await fetch(`https://api.spotify.com/v1/tracks/${currentTrackId}`, {
          headers: {
            Authorization: `Bearer ${spotifyAPI.getAccessToken()}`,
          },
        }).then(
          (res) => res.json(),
          (err) => console.log('Something went wrong!', err),
        );
      } else {
        trackDetails = await spotifyAPI.getMyCurrentPlayingTrack().then(
          (res) => res.body?.item,
          (err) => console.log('Something went wrong!', err),
        );
        await spotifyAPI.getMyCurrentPlaybackState().then(
          (res) => setIsPlayingState(res.body?.is_playing),
          (err) => console.log('Something went wrong!', err),
        );
      }
      setSongDetails(trackDetails);
    };
    fetchSongDetails();
  }, [currentTrackId, spotifyAPI]);

  useEffect(() => {
    if (currentTrackId !== songDetails?.id) {
      setCurrentTrackId(songDetails?.id);
    }
  }, [songDetails]);
  return songDetails;
}

export default useSongDetails;
