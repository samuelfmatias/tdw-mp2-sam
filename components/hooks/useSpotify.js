import { useEffect } from 'react';
import { signIn, useSession } from 'next-auth/react';
import { spotifyAPI } from '../../lib/spotify';

function useSpotify() {
  const { data: session /* status */ } = useSession();
  useEffect(() => {
    if (session) {
      /* BUG FIX - [...nextauth continuation if token is invalid and need to refresh] 
      Desta forma, quando o error existe, somos direcionados para o login
      */
      if (session.error === 'RefreshTokenError') {
        signIn();
      }
      /* Se n existirem erros, definir o token para podermos fazer pedidos */
      spotifyAPI.setAccessToken(session.user.accessToken);
    }
  }, [session]);

  return spotifyAPI;
}

export default useSpotify;
