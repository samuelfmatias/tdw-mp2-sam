import React from 'react';
import { useSession } from 'next-auth/react';
import { AiFillCaretDown } from 'react-icons/ai';

function _ContentUserHeader() {
  const { data: session } = useSession();
  return (
    <>
      {session && (
        <header className='flex items-center absolute top-5 right-8 dark:bg-black dark:text-white text-gray-800 bg-gray-200 shadow-xl space-x-3 opacity-90 dark:hover:opacity-80 hover:opacity-100 cursor-pointer rounded-full p-1 pr-2 h-fit '>
          <img
            className='rounded-full w-10 h-10'
            src={session.user.image}
            alt='Your Profile Picture'
          />
          <h3>{session.user.name}</h3>
          <AiFillCaretDown className='h-5 w-5' />
        </header>
      )}
    </>
  );
}

export default _ContentUserHeader;
